<?php

namespace App\Http\Entity;

use App\Http\Collection\PlayerCollection;

class Game
{
    protected $id;
    protected $players;
    protected $roundCount;

    /**
     * Game constructor.
     */
    public function __construct()
    {
        $this->id = 0;
        $this->players = new PlayerCollection();
    }

    /**
     * @param Player $player
     *
     * @return $this
     */
    public function addPlayer(Player $player)
    {
        $this->players->addPlayer($player);

        return $this;
    }

    public function roundsToPlay($roundCount)
    {
        $this->roundCount = $roundCount;
        $this->players->roundsToPlay($roundCount);

        return $this;
    }
}
