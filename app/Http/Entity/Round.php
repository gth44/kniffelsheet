<?php

namespace App\Http\Entity;

class Round
{
    /**
     * @var int
     */
    protected $ones = null;

    /**
     * @var int
     */
    protected $twos = null;

    /**
     * @var int
     */
    protected $threes = null;

    /**
     * @var int
     */
    protected $fours = null;

    /**
     * @var int
     */
    protected $fives = null;

    /**
     * @var int
     */
    protected $sixes = null;

    /**
     * @var int
     */
    protected $threePash = null;

    /**
     * @var int
     */
    protected $fourPash = null;

    /**
     * @var int
     */
    protected $fullHouse = null;

    /**
     * @var int
     */
    protected $minorStreet = null;

    /**
     * @var int
     */
    protected $bigStreet = null;

    /**
     * @var int
     */

    protected $kniffel = null;

    /**
     * @var int
     */
    protected $chance = null;
}
