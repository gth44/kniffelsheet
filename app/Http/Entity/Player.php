<?php

namespace App\Http\Entity;

use App\Http\Collection\RoundCollection;

class Player
{
    /**
     * @var string
     */
    protected $playerName;

    /**
     * @var RoundCollection
     */
    protected $rounds;

    /**
     * Player constructor.
     *
     * @param $playerName
     */
    public function __construct($playerName)
    {
        $this->playerName = $playerName;
        $this->rounds = new RoundCollection();
    }

    /**
     * @param $roundCount
     *
     * @return $this
     */
    public function roundsToPlay($roundCount)
    {
        $this->rounds->addRounds($roundCount);

        return $this;
    }
}
