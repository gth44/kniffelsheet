<?php

namespace App\Http\Collection;

use App\Http\Entity\Player;

class PlayerCollection
{
    /**
     * @var array
     */
    protected $players = [];

    /**
     * @param Player $player
     *
     * @return $this
     *
     */
    public function addPlayer(Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * @param $roundCount
     *
     * @return $this
     */
    public function roundsToPlay($roundCount)
    {
        foreach ($this->players as $player)
        {
            $player->roundsToPlay($roundCount);
        }

        return $this;
    }
}
