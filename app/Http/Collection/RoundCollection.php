<?php

namespace App\Http\Collection;

use App\Http\Entity\Round;

class RoundCollection
{
    /**
     * @var array
     */
    protected $rounds = [];

    /**
     * RoundCollection constructor.
     *
     * @param $roundCount
     */
    public function __construct($roundCount = 0)
    {
        for ($i = 0; $i < $roundCount; $i++)
        {
            $this->rounds[] = new Round();
        }
    }

    /**
     * @return $this
     */
    public function addRound()
    {
        $this->rounds[] = new Round();

        return $this;
    }

    /**
     * @param $roundCount
     *
     * @return $this
     */
    public function addRounds($roundCount)
    {
        for($i = 0; $i < $roundCount; $i++)
        {
            $this->addRound();
        }

        return $this;
    }
}
