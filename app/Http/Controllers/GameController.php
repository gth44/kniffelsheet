<?php

namespace App\Http\Controllers;

use App\Http\Entity\Game;
use App\Http\Entity\Player;

class GameController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        $player1 = new Player('testplayer1');
        $player2 = new Player('testplayer2');

        $game = new Game(3);
        $game->addPlayer($player1);
        $game->addPlayer($player2);
        $game->roundsToPlay(3);

        echo '<pre>';
        var_dump($game);
    }
}
